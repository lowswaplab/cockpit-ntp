
# cockpit-ntp

NTP status and configuration for [Cockpit](https://cockpit-project.org/).

## URL 

https://gitlab.com/lowswaplab/cockpit-ntp

## Copyright

Copyright 2018 Low SWaP Lab [lowswaplab.com](https://lowswaplab.com/)

