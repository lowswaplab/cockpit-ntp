
// cockpit-ntp
// https://gitlab.com/lowswaplab/cockpit-ntp
// Copyright 2018 Low SWaP Lab lowswaplab.com

  function chrony_run()
    {
    var proc;

    proc = cockpit.spawn(["chronyc", "sources"]);
    proc.stream(ntp_output_sources);

    appname.innerHTML = "chrony";
    output_sources.innerHTML = "";

    proc = cockpit.spawn(["chronyc", "clients"], {"superuser": "try"});
    proc.stream(ntp_output_clients);

    appname.innerHTML = "chrony";
    output_clients.innerHTML = "";
    }

  function ntp_output_sources(data)
    {
    output_sources.append(document.createTextNode(data));
    }

  function ntp_output_clients(data)
    {
    output_clients.append(document.createTextNode(data));
    }

  function ntp_run()
    {
    chrony_run();
    }

var appname;
var output_sources;
var output_clients;

document.querySelector(".container-fluid").style["max-width"] = "800px";

appname = document.getElementById("appname");
output_sources = document.getElementById("output-sources");
output_clients = document.getElementById("output-clients");

cockpit.transport.wait(function() { });

ntp_run();

window.setInterval(function(){ ntp_run(); }, 30000);

